package com.lytvynuk.test.service;

import com.lytvynuk.test.dto.BookDto;
import com.lytvynuk.test.repository.BookRepository;
import com.lytvynuk.test.transformer.BookTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookTransformer bookTransformer;

    public List<BookDto> findAllBooks() {
        return bookRepository.findAll().stream()
                .map(this.bookTransformer::toDto)
                .collect(Collectors.toList());
    }

}
