package com.lytvynuk.test.service;

import com.lytvynuk.test.dto.AuthorDto;
import com.lytvynuk.test.repository.AuthorRepository;
import com.lytvynuk.test.transformer.AuthorTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private AuthorTransformer authorTrasformer;

    public List<AuthorDto> findAllAuthors() {
        return authorRepository.findAll().stream()
                .map(this.authorTrasformer::toDto)
                .collect(Collectors.toList());
    }
}
