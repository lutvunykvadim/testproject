package com.lytvynuk.test.controller;

import com.lytvynuk.test.dto.AuthorDto;
import com.lytvynuk.test.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorController {
    @Autowired
    private AuthorService authorService;
    @GetMapping("/api/v1/authors")
    public List<AuthorDto> getAllAuthors(){return authorService.findAllAuthors();}
}
