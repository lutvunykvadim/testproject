package com.lytvynuk.test.transformer;

import com.lytvynuk.test.dto.AuthorDto;
import com.lytvynuk.test.entity.Author;
import com.lytvynuk.test.entity.Book;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthorTransformer {
    public AuthorDto toDto(Author author) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(author.getId());
        authorDto.setName(author.getName());

        List<String> bookNames = author.getBooks().stream()
                .map(Book::getName)
                .collect(Collectors.toList());
        authorDto.setNameOfBook(bookNames);
        return authorDto;
    }
}
