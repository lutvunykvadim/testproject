package com.lytvynuk.test.transformer;

import com.lytvynuk.test.dto.BookDto;
import com.lytvynuk.test.entity.Author;
import com.lytvynuk.test.entity.Book;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookTransformer {

    public BookDto toDto(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());

        List<String> authorNames = book.getAuthors().stream()
                .map(Author::getName)
                .collect(Collectors.toList());
        bookDto.setNameOfAuthor(authorNames);
        return bookDto;
    }
}
