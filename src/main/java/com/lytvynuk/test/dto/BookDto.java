package com.lytvynuk.test.dto;

import java.util.List;

public class BookDto {
    private Integer id;
    private String name;
    private List<String> nameOfAuthor;

    public List<String> getNameOfAuthor() {
        return nameOfAuthor;
    }

    public void setNameOfAuthor(List<String> nameOfAuthor) {
        this.nameOfAuthor = nameOfAuthor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
